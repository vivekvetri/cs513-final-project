SELECT count(FMID) FROM farmers_markets WHERE FMID is NULL;

SELECT COUNT(distinct FMID)-COUNT(FMID) FROM farmers_markets;

SELECT count(MarketName) FROM farmers_markets WHERE MarketName is NULL;

SELECT MarketName, COUNT(*) FROM farmers_markets GROUP BY MarketName HAVING COUNT(*) > 1;

SELECT Website, count(website) FROM farmers_markets WHERE OtherMedia = Website AND OtherMedia !="";

SELECT Facebook, count(Facebook) FROM farmers_markets WHERE OtherMedia = Facebook AND OtherMedia !="";

SELECT Twitter, count(Twitter) FROM farmers_markets WHERE OtherMedia = Twitter AND OtherMedia !="";

SELECT Youtube, count(Youtube) FROM farmers_markets WHERE OtherMedia = Youtube AND OtherMedia !="";

UPDATE farmers_markets SET OtherMedia ="" WHERE FMID IN (SELECT FMID FROM farmers_markets WHERE OtherMedia = Facebook AND Othermedia != "");

UPDATE farmers_markets SET OtherMedia ="" WHERE FMID IN (SELECT FMID FROM farmers_markets WHERE OtherMedia = Website AND Othermedia != "");

UPDATE farmers_markets SET OtherMedia ="" WHERE FMID IN (SELECT FMID FROM farmers_markets WHERE OtherMedia = Twitter AND Othermedia != "");

UPDATE farmers_markets SET OtherMedia ="" WHERE FMID IN (SELECT FMID FROM farmers_markets WHERE OtherMedia = Youtube AND Othermedia != "");

SELECT FMID, MarketName, Season1StartDate, Season1EndDate FROM farmers_markets WHERE Season1StartDate> Season1EndDate
AND Season1EndDate != ""  AND date(Season1StartDate);

UPDATE farmers_markets SET Season1StartDate = Season1EndDate, Season1EndDate = Season1StartDate  
WHERE FMID = (SELECT FMID FROM farmers_markets WHERE Season1StartDate> Season1EndDate AND Season1EndDate != "" AND date(Season1StartDate));

SELECT FMID, MarketName, Season1StartDate, Season1EndDate FROM farmers_markets WHERE FMID = 1011959